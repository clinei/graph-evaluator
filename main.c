#include <stdio.h>
#include <stdlib.h>

struct node {
  struct node* ports;
  unsigned short numports;
  short* buf;
  unsigned int buflen;
  char numevalled;
  void(*func)(struct node*);
  //void(*func)(short*, unsigned int, struct node*);
};

void node_eval(struct node* self) {
  ++self->numevalled;
  for (short i = 0; i < self->numports; i++) {
    struct node* port_node = self->ports + i;
    if ((port_node->numevalled - self->numevalled) < 0) {
      node_eval(self->ports + i);
    }
  }
  (self->func)(self);
}

void mult(struct node* self) {
  //short* cbuf = malloc(sizeof(short) * len);
  //node_eval(ports + 0, cbuf, len);
  unsigned int len = self->buflen;
  for (unsigned int i = 0; i < len; i++) {
    self->buf[i] = self->ports[0].buf[i];
  }
  //node_eval(ports + 1, cbuf, len);
  for (unsigned int i = 0; i < len; i++) {
    self->buf[i] *= self->ports[1].buf[i];
  }
  //free(cbuf);
}

void sawtooth(struct node* self) {
  for (unsigned int i = 0; i < self->buflen; i++) {
    self->buf[i] = (short) i;
  }
}

int main() {
  short* buf = malloc(sizeof(short) * 256);

  //instead of allocating a node for every node i allocate a node list for easier freeing
  struct node* nodelist = malloc(sizeof(struct node) * 2);
  struct node* saw = nodelist + 0;
  struct node* ring = nodelist + 1;

  //same list magic as before
  short* buflist = malloc(sizeof(short) * 512);

  //todo: refactor into an allocator function, somehow.
  saw->func = sawtooth;
  saw->buf = buflist + 0;
  saw->buflen = 256;
  saw->numports = 0;
  saw->numevalled = 0;
  ring->func = mult;
  ring->buf = buflist + 256;
  ring->buflen = 256;
  ring->ports = malloc(sizeof(struct node) * 2);
  ring->ports[0] = *saw;
  ring->ports[1] = *saw;
  ring->numports = 2;
  ring->numevalled = 0;

  node_eval(ring);
  printf("%d %d %d\n", ring->buf[1], ring->buf[5], ring->buf[7]);
  free(buflist); free(nodelist);
}

